"""Configuration for a new project."""

# use hyphens for word seperation
project_name = "project-name"

gitlab_base_url = "https://gitlab.mueller5.eu/python"
gitlab_pages_base_url = "https://python.pages.mueller5.eu"

author = "Stephan Müller"

license = 'MIT'

docs = True
python_version = '3'

tests = True
docker = True
docker_integration_tests = True

# None to deactivate
database_management = 'postgres'
