#!/usr/bin/env python3
#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""Generate new python project."""

import os
import logging
from datetime import date
import shutil
import argparse
from jinja2 import Template

import config
from licenses import LICENSES

LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", "INFO")
logging_format = "%(levelname)8s %(asctime)s %(message)s"
logging.basicConfig(level=logging.getLevelName(LOGGING_LEVEL), format=logging_format)

logger = logging.getLogger(__name__)

__dirname__ = os.path.dirname(os.path.abspath(__file__))

parser = argparse.ArgumentParser(description="Creates a new python project skeleton")
parser.add_argument("base_dir", help="Path where project folder should be created")
parser.add_argument("-f", "--force", action="store_true", help="Force recreation of files")


args = parser.parse_args()

PROJECT_DIR = os.path.join(args.base_dir, config.project_name)

config.year = date.today().year

pythonic_project_name = config.project_name.replace("-", "_")
project_name_uppercase = ""
for word in config.project_name.split("-"):
    project_name_uppercase += word[0].upper() + word[1:]

template = Template(open(os.path.join(__dirname__, "template", "license_header.j2")).read())
license_header = template.render(author=config.author, year=date.today().year, license=config.license)


def process_template(file, dest_file=None):
    """General function for replacing template files."""
    if dest_file is None:
        dest_file = file

    template = Template(open(os.path.join(__dirname__, "template", file + ".j2")).read())
    content = template.render(
        author=config.author,
        year=date.today().year,
        license=config.license,
        project_name=config.project_name,
        pythonic_project_name=pythonic_project_name,
        gitlab_base_url=config.gitlab_base_url,
        gitlab_pages_base_url=config.gitlab_pages_base_url,
        docs=config.docs,
        python_version=config.python_version,
        tests=config.tests,
        docker=config.docker,
        license_header=license_header,
        project_name_uppercase=project_name_uppercase,
        database_management=config.database_management
    )
    open(os.path.join(PROJECT_DIR, dest_file), "w+").write(content + "\n")


if not args.force and os.path.isdir(PROJECT_DIR):
    raise Exception("Path %s already exists. Use --force to overwrite." % (PROJECT_DIR,))

os.makedirs(PROJECT_DIR, exist_ok=True)
os.makedirs(os.path.join(PROJECT_DIR, pythonic_project_name), exist_ok=True)
process_template("default.py", os.path.join(pythonic_project_name, "__init__.py"))
process_template("configuration.py", os.path.join(pythonic_project_name, "configuration.py"))
shutil.copy(os.path.join(__dirname__, "template", ".gitignore"), os.path.join(PROJECT_DIR, ".gitignore"))
shutil.copy(os.path.join(__dirname__, "template", ".gitignore"), os.path.join(PROJECT_DIR, ".dockerignore"))
for f in [".gitlab-ci.yml", "README.md", "requirements.txt"]:
    process_template(f)
process_template("licenses/" + LICENSES[config.license], "LICENSE")

if config.tests:
    os.makedirs(os.path.join(PROJECT_DIR, "tests"), exist_ok=True)
    process_template(os.path.join("tests", "__init__.py"))
    process_template(os.path.join("tests", "test.py"), os.path.join("tests", "test_" + pythonic_project_name + ".py"))

if config.docs:
    os.makedirs(os.path.join(PROJECT_DIR, "docs", "api"), exist_ok=True)
    for f in ["docs/Makefile", "docs/index.rst", "docs/conf.py"]:
        process_template(f)
    process_template(os.path.join("docs", "api", "project_name.rst"),
                     os.path.join("docs", "api", pythonic_project_name + ".rst"))

if config.docker:
    for f in ["app.py", "Dockerfile"]:
        process_template(f)

if config.database_management is not None:
    process_template('docker-compose.yml')

if config.database_management == 'postgres':
    os.makedirs(os.path.join(PROJECT_DIR, 'database', 'versions'), exist_ok=True)
    for file in ['alembic.ini', 'script.py.mako']:
        shutil.copy(os.path.join(__dirname__, 'template', 'database', file),
                    os.path.join(PROJECT_DIR, 'database', file))
        process_template(os.path.join('database', 'env.py'))
