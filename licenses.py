#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""License definitions. Each license requires a license template in template/licenses/LICENSE.j2."""

LICENSES = {
    'MIT': 'MIT',
}
